---
title: Support
---

Support
=======

Learn how best to raise an issue to Yapily.

* * *

Raising Issues
--------------

When raising issues the following information quickly helps Yapily diagnose the issues you face:

1.  **The application name or id**: Some clients have several Yapily applications so identifying the application that has the problem allows Yapily to identify the issue faster
2.  **The series of endpoints called leading up to the error**: While the error itself may be instructive enough to solve an issue, knowing what you were attempting to do in your application gives us an opportunity to rule out any trivial issues
3.  **The `tracingId` for the relevant requests**: To quickly find your failing request apart from all other Yapily traffic, this id is the most important piece of information to share with Yapily
4.  When the `tracingId` is not stored on your side, the timestamp of the request and other unique ids in the path and/or the body will help Yapily find your requests

Yapily Error Handling
---------------------

As mentioned above, one of the most important fields to raise with our support team is the `tracingId` which allows Yapily to immediately find your request. This is returned to in the [ApiResponseError](/#) object when there are any errors returned by Yapily or the `Institution` when making requests:

    
    {
        "error": {
            "code": 424,
            "tracingId": "node09c1ljiqbvqbr1e64z116fz7jo0",
            "status": "FAILED_DEPENDENCY",
            "source": "INSTITUTION",
            "message": "Error from Institution. We can help you on https://support.yapily.com/"
        }
    }
    

If there is an issue with a request to a particular Institution, Yapily also provides the response from the `Institution` in the `institutionError` object if it is available providing more information:

    
    {
        "error": {
            "code": 424,
            "tracingId": "node09c1ljiqbvqbr1e64z116fz7jo0",
            "status": "FAILED_DEPENDENCY",
            "source": "INSTITUTION",
            "message": "Error from Institution. We can help you on https://support.yapily.com/",
            "institutionError": {
                "httpStatusCode": 400,
                "errorMessage": "{\"Code\":\"UK.OBIE.Signature.Malformed\",\"Message\":\"invalid_iss_claim: Invalid iss claim. Got org_id/statement_id. Expected 001580000103UArAAM/rapTYmFWJcXfdo2EvksDUx\",\"Errors\":[{\"ErrorCode\":\"UK.OBIE.Signature.Malformed\",\"Message\":\"invalid_iss_claim: Invalid iss claim. Got org_id/statement_id. Expected 001580000103UArAAM/rapTYmFWJcXfdo2EvksDUx\",\"Field\":\"x-jws-signature\"}]}"
            }
        }
    }
    

Failing Authorisations
----------------------

A key part of the Open Banking journey is to redirect the PSU to the `Institution` where they provide their authorisation. As explained in [handling the callback parameters](/#) and [handling the redirect-url response](/#), in the event of a failure to authorise or an issue with `Institution`, the failure response is returned as a query parameter.

Yapily strongly recommends logging the query parameters received on either the `callback` or the `redirectUrl` (depending on your use case) for all requests. While Yapily logs all the redirects received for customers using Yapily's authorisation service (`https://auth.yapily.com/`), if you are using your own `redirectUrl`, there is no way for Yapily to log any information on inbound redirect back to your application.

Similarly, if there is no redirect back to your application either via. Yapily or directly using your `redirectUrl`, Yapily also has no way of identifying whether the authorisation was successful or not. Yapily recommends that in this scenario, you prompt the PSU to attempt to re-authorise again.

Further Help
------------

If you can't find what you're looking for in the documentation, reach out at [support@yapily.com](mailto:support@yapily.com?subject=Documentation Query)