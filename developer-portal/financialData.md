[](#)

Get Financial Data
==================

This tutorial walks you through each of the steps required to obtain a user's account and transaction data.

This tutorial walks through how to access the account information and transactions in the UK with the Modelo Sandbox. Each step contains links to further reading for more detailed information.


* * *

1\. Get Institutions
--------------------

**Optional** - This step is optional as it is entirely possible to access account data without retrieving the `Institution` list, however it is often required to display a list of supported banks for your customer to choose which bank they would like to share their data from.

**Note:** All requests made to the Yapily API require [basic authentication](/api/#authentication).

To display this list, use [GET Institutions](/api/#get-institutions):

Request:

    
    curl --location --request GET 'https://api.yapily.com/institutions' \
      --header 'Authorization: Basic {authToken}'
    

Response:

    
    {
      "meta": {
        "tracingId": "acbb76db4ab8f4ac7f039d000456c13f",
        "count": 1
      },
      "data": [
        {
          "id": "modelo-sandbox",
          "name": "Modelo Sandbox",
          "fullName": "Modelo Sandbox",
          "countries": [
            {
              "displayName": "United Kingdom",
              "countryCode2": "GB"
            }
          ],
          "environmentType": "SANDBOX",
          "credentialsType": "OPEN_BANKING_UK_AUTO",
          "media": [
            {
              "source": "https://images.yapily.com/image/ce2bfdbf-1ae2-4919-ab7b-e8b3d5e93b36?size=0",
              "type": "icon"
            },
            {
              "source": "https://images.yapily.com/image/ca502f24-d6df-4785-b4b8-1034b100af77?size=0",
              "type": "logo"
            }
          ],
          "features": [
            "INITIATE_ACCOUNT_REQUEST",
            "ACCOUNT_REQUEST_DETAILS",
            "EXISTING_PAYMENTS_DETAILS",
            "ACCOUNT_BALANCES",
            "CREATE_BULK_PAYMENT",
            "ACCOUNT_PERIODIC_PAYMENTS",
            "ACCOUNT_STATEMENTS",
            "INITIATE_BULK_PAYMENT",
            "ACCOUNT_STATEMENT",
            "ACCOUNT",
            "INITIATE_DOMESTIC_PERIODIC_PAYMENT",
            "INITIATE_SINGLE_PAYMENT_SORTCODE",
            "ACCOUNT_DIRECT_DEBITS",
            "ACCOUNTS",
            "ACCOUNT_TRANSACTIONS",
            "EXISTING_PAYMENT_INITIATION_DETAILS",
            "CREATE_DOMESTIC_SINGLE_PAYMENT",
            "INITIATE_DOMESTIC_SINGLE_PAYMENT",
            "ACCOUNT_STATEMENT_FILE",
            "CREATE_INTERNATIONAL_SINGLE_PAYMENT",
            "IDENTITY",
            "CREATE_DOMESTIC_SCHEDULED_PAYMENT",
            "INITIATE_DOMESTIC_SCHEDULED_PAYMENT",
            "CREATE_SINGLE_PAYMENT_SORTCODE",
            "ACCOUNT_TRANSACTIONS_WITH_MERCHANT",
            "INITIATE_INTERNATIONAL_SINGLE_PAYMENT",
            "PERIODIC_PAYMENT_FREQUENCY_EXTENDED",
            "ACCOUNT_SCHEDULED_PAYMENTS",
            "CREATE_DOMESTIC_PERIODIC_PAYMENT"
          ]
        }
      ]
    }
    

*   Each `Institution` returned contains a logo, name, id and a list of supported features. As we want to retrieve account information and transactions, the institution needs to support the `ACCOUNTS` and `ACCOUNT_TRANSACTIONS` features.
    
*   In your app, you should ask the user to select the bank they wish to share their account data from.
    
*   Once selected, the `Institution` `id` should be passed in the next step.
    

[Further reading ↗](/guides/applications/institutions/)

* * *

2\. Authorisation
-----------------

![](/assets/images/guides/AIS_permission-f51ac374.png)

After obtaining the `institution-id` and confirming that the bank supports the ability access accounts information and transactions, the next step is to generate an `authorisation-url` that is used to redirect the user to their chosen bank and to authorise the account request.

When building your live application, Yapily recommends that the `authorisationUrl` is surfaced to the user on a page in your application where an overview of the request is displayed to the user before sending them directly to the bank to approve the request to share their account information. This is to help to reduce the the likeliness of unauthorised requests by providing all the key information to the user upfront. The `authorisationUrl` can be set as the link in a button that will "Confirm" that the user agrees with the overview.

To do this, execute [Create Account Authorisation](/api/#create-account-authorisation):

Request:

    
    curl --location --request POST 'https://api.yapily.com/account-auth-requests' \
     --header 'Content-Type: application/json' \
     --header 'Authorization: Basic {authToken}' \
     --data-raw '{
       "applicationUserId": "account-data-and-transactions-tutorial",
       "institutionId": "modelo-sandbox",
       "callback": "https://display-parameters.com/"
     }'
    

Response:

    
    {
      "meta": {
        "tracingId": "ceab45e96f852afdb59ec8eaf8f93594"
      },
      "data": {
        "id": "979f8a58-c955-46b5-b25d-bdc447c28a80",
        "userUuid": "c78035e1-3140-44f7-9539-527785069356",
        "applicationUserId": "account-data-and-transactions-tutorial",
        "institutionId": "modelo-sandbox",
        "status": "AWAITING_AUTHORIZATION",
        "createdAt": "2021-03-04T11:05:17.234Z",
        "featureScope": [
          "ACCOUNT_STATEMENT_FILE",
          "ACCOUNT_STATEMENTS",
          "ACCOUNT_BALANCES",
          "ACCOUNT_SCHEDULED_PAYMENTS",
          "ACCOUNT_STATEMENT",
          "ACCOUNT_TRANSACTIONS_WITH_MERCHANT",
          "ACCOUNT_TRANSACTIONS",
          "ACCOUNT_DIRECT_DEBITS",
          "IDENTITY",
          "ACCOUNT",
          "ACCOUNTS",
          "ACCOUNT_PERIODIC_PAYMENTS"
        ],
        "state": "e539228113de43b3a3d4c3b9944620d3",
        "institutionConsentId": "aac-74e55df8-a673-48b4-b8ae-3e8d0e45080f",
        "authorisationUrl": "{authorisationUrl}",
        "qrCodeUrl": "https://images.yapily.com/image/d4113d96-9d6c-4e69-a840-2e327cff1b1d/1614855917?size=0"
      }
    }
    

![](/assets/images/guides/AIS_bankScreen-2a5cfde7.png)

*   The [AuthorisationRequestResponse](/api/#tocS_AuthorisationRequestResponse) returned includes the `authorisationUrl` that you should redirect the user to.
    
*   Note that the `featureScope` object defines every feature that you will be able to access for each account the user authorises using the `consentToken` that will be issued once they authorise the request.
    
*   The user will be asked to login and authorise the account sharing request in their bank. For this tutorial we use the [sandbox credentials](/guides/applications/institutions/sandbox/): `mits` / `mits`.
    
*   Access to the user's account information and all other features that the `Consent` was created for will be for a maximum of 90 days by default before requiring re-authorisation from the user, however, there are several cases where the length of time that the user's data can be accessed may vary. See the further reading links below to find out more.
    
*   Upon completion the user will be redirected back to the `callback` url supplied in the request. In our example we are using `https://display-parameters.com/`, which will display the parameters returned with the redirect. The most important of which is the `consentToken`, which is used in the next steps to access the accounts information and transactions.
    

* * *

3\. Get the Accounts
--------------------

Once the user authorises the account request and you have the `consentToken`, you are able to access the account information that the user has consented to share.

To do this, execute [Get Accounts](/api/#get-accounts):

Request:

    
    curl --location --request GET 'https://api.yapily.com/accounts' \
      --header 'Consent: {consentToken}' \
      --header 'Authorization: Basic {authToken}'
    

Response:

    
    {
      "meta": {
        "tracingId": "14df65068fd38689b392ad39602143cb",
        "count": 1
      },
      "data": [
        {
          "id": "700004000000000000000007",
          "type": "Personal - Current",
          "balance": -99765867307.74,
          "currency": "GBP",
          "usageType": "PERSONAL",
          "accountType": "CURRENT",
          "nickname": "xxxx0009",
          "accountNames": [
            {
              "name": "Mr. Mitsuhirato"
            }
          ],
          "accountIdentifications": [
            {
              "type": "PAN",
              "identification": "1234000000000001"
            }
          ],
          "accountBalances": [
            {
              "type": "EXPECTED",
              "dateTime": "2021-03-04T11:08:39.291Z",
              "balanceAmount": {
                "amount": -99765867307.74,
                "currency": "GBP"
              },
              "creditLineIncluded": false,
              "creditLines": []
            }
          ]
        }
      ]
    }
    

![](/assets/images/guides/AIS_GetAccounts-7f9e30e7.png)

*   The response contains an array of [Account](/api/#tocS_Account) objects for each account the user gave their consent to at the bank.
    
*   Yapily normalises the response for every `Institution` to ensure that the data that is provided from the customer's account can be handled in the same way.
    
*   The `id` refers to the account id that the `Institution` recognises to obtain the account transactions
    

* * *

4\. Get the Transactions
------------------------

Using the `consentToken` that was created in step 2 along with the `account-id` in step 3, you can also obtain the transactions for the specified account.

To do this, execute [Get Account Transactions](/api/#get-account-transactions):

Request:

    
    curl --location --request GET 'https://api.yapily.com/accounts/700004000000000000000007/transactions' \
      --header 'Consent: {consentToken}' \
      --header 'Authorization: Basic {authToken}'
    

Response:

    
    {
      "meta": {
        "tracingId": "0d9835a9c83204f8c0607ddfef961bab",
        "count": 760,
        "pagination": {
          "totalCount": 760,
          "self": {
            "limit": 1000,
            "sort": "-date",
            "offset": 0
          }
        }
      },
      "data": [
        {
          "id": "15f5b8b3-d8ca-422a-af4e-2538546788e8",
          "date": "2021-02-26T11:07:41.79Z",
          "bookingDateTime": "2021-02-26T11:07:41.79Z",
          "valueDateTime": "2021-02-26T11:07:41.79Z",
          "status": "BOOKED",
          "amount": -0.99,
          "currency": "GBP",
          "transactionAmount": {
            "amount": -0.99,
            "currency": "GBP"
          },
          "reference": "7",
          "description": "Debit for Payment Id: sdp-1-58718033-969e-4941-832b-3ad7b76c3e71",
          "transactionInformation": [
            "Debit for Payment Id: sdp-1-58718033-969e-4941-832b-3ad7b76c3e71"
          ],
          "isoBankTransactionCode": {
            "domainCode": {
              "code": "PMNT",
              "name": "Payments"
            },
            "familyCode": {
              "code": "ICDT",
              "name": "Issued Credit Transfers"
            },
            "subFamilyCode": {
              "code": "DMCT",
              "name": "Domestic Credit Transfer"
            }
          },
          "proprietaryBankTransactionCode": {
            "code": "PMT",
            "issuer": "modelo-sandbox"
          },
          "balance": {
            "type": "CLOSING_AVAILABLE",
            "balanceAmount": {
              "amount": 99765867307.74,
              "currency": "GBP"
            }
          },
          "enrichment": {
            "transactionHash": {
              "hash": "54be0baf91aaa732cd8b6bacc6396042.1"
            }
          }
        },
        ...
      ],
      "links": {
        "self": "https://api.yapily.com/accounts/700004000000000000000007/transactions?from&before&limit=1000&sort=-date&offset=0&cursor"
      }
    }
    

![](/assets/images/guides/AIS_GetTransactions-8735d44c.png)

*   By default, Yapily will limit the results to the most recent 1000 transactions
    
*   It is possible to add query parameters to this request in order to pull historic data or specific ranges and to obtain a paginated set of results
    
*   While every bank does not provide the transaction id, one of the way Yapily improves the experience is through our `Enrichment` which provides a `transactionHash` which can be leveraged.
    

* * *

Congratulations, if you got this far, you've just pulled account information and transactions via Yapily.

Related Tutorials
-----------------

*   [Single Domestic Payments](/#)