---
title: Dashboard Overview
---

Dashboard
=========

Yapily's powerful dashboard provides a single interface for managing your Yapily integrations.

Create an Application
---------------------

1.  Register or log into the Yapily Dashboard
    
2.  Click My Applications
    
3.  Click Add new to create an application and fill in the name field.
    
4.  Click Save and download the file containing your application's by clicking Download Secret:
    

![](https://kb.yapily.com//assets/images/product/downloadApplicationSecret2-dee6d825.png)

This file containing your application credential is only generated when your application is created. If you lose the application secret, you will need to click the Revoke Secret button. This will create a new application secret (invalidating any requests using the old secret), and generate a new credentials file.

![](https://kb.yapily.com//assets/images/product/credentials-2e1e55a2.png)

Add a sandbox
-------------

A sandbox is a test environment provided by a bank allowing third parties to make connections to that bank simulating the real Open Banking experience. For each sandbox you wish to use, you will need to create a developer account with that bank and use them to configure the sandbox once you add them to the dashboard:

![](https://kb.yapily.com//assets/images/product/sandbox-cf6998a9.png)

Yapily recommends the use of preconfigured sandboxes to bypass the registration process completely however, if you choose to set up your own developer accounts with your chosen financial institutions, please follow the relevant guide for each institution.

### Preconfigured Sandboxes

A preconfigured sandbox is a sandbox that has already been configured using Yapily's developer credentials. This method if the quickest way to use Yapily's APIs as you do not have to go through the process of obtaining credentials for each of the banks to configure a sandbox. You can use any of Yapily's sandboxes by filtering the list of Institutions on the Dashboard:

![](https://kb.yapily.com//assets/images/product/preconfigured-f0e6cca4.png)

You can test your application using one of these options:

*   Launch the Simulator from the dashboard.
*   Use the Postman collection

### Sandbox User Credentials

Depending on which preconfigured Sandbox you have chosen, you will need to authenticate with the corresponding institution using the appropriate sandbox test credentials in order to simulate a user's login to the bank and authorise your application's access to the data.

*   Using the preconfigured sandboxes does not require registration with external entities
*   While Yapily offers a range of sandboxes, we do NOT maintain the quality or functionality of any sandbox with the following exceptions:
    *   Modelo Sandbox - Provided by the Open Banking regulators and is actively maintained
    *   Yapily Mock - Provided by Yapily and facilitates automated testing
*   For production data, you will need to register with on the institution's developer dashboard in order to receive the relevant credentials to configure that institution for your application in the Yapily dashboard.