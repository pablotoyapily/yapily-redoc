[](#)

Account and Balance Information
===============================

Learn more about the information returned on Accounts and Balances

Balance Types
-------------

`account/data/accountBalances[]/type`

Yapily has a defined set of Balance Types and those reported for an account will depend on the information shared by the ASPSP.

Each Balance Type has a specific purpose:

Balance Type

Description

`CLOSING_AVAILABLE`

Closing balance of amount of money that is at the disposal of the account owner on the date specified

`CLOSING_BOOKED`

Balance of the account at the end of the pre-agreed account reporting period. It is the sum of the opening booked balance at the beginning of the period and all entries booked to the account during the pre-agreed account reporting period.

`CLOSING_CLEARED`

Closing balance of amount of money that is cleared on the date specified.

`EXPECTED`

Balance, composed of booked entries and pending items known at the time of calculation, which projects the end of day balance if everything is booked on the account and no other entry is posted.

`FORWARD_AVAILABLE`

Forward available balance of money that is at the disposal of the account owner on the date specified.

`INFORMATION`

Balance for informational purposes.

`INTERIM_AVAILABLE`

Available balance calculated in the course of the account servicer's business day, at the time specified, and subject to further changes during the business day. The interim balance is calculated on the basis of booked credit and debit items during the calculation time/period specified.

`INTERIM_BOOKED`

Balance calculated in the course of the account servicer's business day, at the time specified, and subject to further changes during the business day. The interim balance is calculated on the basis of booked credit and debit items during the calculation time/period specified.

`INTERIM_CLEARED`

Cleared balance calculated in the course of the account servicer's business day, at the time specified, and subject to further changes during the business day.

`OPENING_AVAILABLE`

Opening balance of amount of money that is at the disposal of the account owner on the date specified.

`OPENING_BOOKED`

Book balance of the account at the beginning of the account reporting period. It always equals the closing book balance from the previous report.

`OPENING_CLEARED`

Opening balance of amount of money that is cleared on the date specified.

`PREVIOUSLY_CLOSED_BOOKED`

Balance of the account at the previously closed account reporting period. The opening booked balance for the new period has to be equal to this balance. Usage: the previously booked closing balance should equal (inclusive date) the booked closing balance of the date it references and equal the actual booked opening balance of the current date.

`OTHER`

Used when an unrecognised enum is parsed

`UNKNOWN`

Used as a Yapily default

Main Balance
------------

`account/data/balance`

We recommend the use of the typed balances (`accountBalances[]`) wherever possible and according to your business needs.

However, the Yapily `account` resource also includes a ‘main balance’ (`balance`) which may be used as a fallback.

After forming the set of Yapily Balance Types, one of the balances will additionally be mapped to this ‘main balance’ according to a pre-defined priority (where the first balance that is available from the ASPSP is mapped).

### Standard Mapping (in Priority Order)

*   INTERIM\_BOOKED  
    
*   OPENING\_BOOKED  
    
*   CLOSING\_BOOKED  
    
*   EXPECTED  
    
*   INTERIM\_AVAILABLE  
    
*   OPENING\_AVAILABLE  
    
*   CLOSING\_AVAILABLE  
    
*   FORWARD\_AVAILABLE  
    
*   PREVIOUSLY\_CLOSED\_BOOKED  
    
*   INTERIM\_CLEARED  
    
*   OPENING\_CLEARED  
    
*   CLOSING\_CLEARED  
    
*   INFORMATION  
    
*   AUTHORISED  
    
*   OTHER
*   UNKNOWN

  

Bank Specific Mapping

### Santander

*   INTERIM\_AVAILABLE
*   INTERIM\_CLEARED
*   INTERIM\_BOOKED
*   OPENING\_AVAILABLE
*   OPENING\_CLEARED
*   OPENING\_BOOKED
*   FORWARD\_AVAILABLE
*   EXPECTED INFORMATION
*   PREVIOUSLY\_CLOSED\_BOOKED
*   CLOSING\_AVAILABLE
*   CLOSING\_CLEARED
*   CLOSING\_BOOKED

### Halifax

*   INTERIM\_BOOKED
*   INTERIM\_AVAILABLE
*   INTERIM\_CLEARED
*   OPENING\_BOOKED
*   OPENING\_AVAILABLE
*   OPENING\_CLEARED
*   FORWARD\_AVAILABLE
*   EXPECTED
*   INFORMATION
*   PREVIOUSLY\_CLOSED\_BOOKED
*   CLOSING\_BOOKED
*   CLOSING\_AVAILABLE
*   CLOSING\_CLEARED

Balance Signing
---------------

`account/data/balance`

`account/data/accountBalances[]/balanceAmount/amount`

Balance amounts are signed according to whether the associated balance is positive or negative:

*   Where a positive number indicates that the customer is holding funds / has funds available to them
    
*   Where negative number indicates that the customer owes funds to the bank
    

This is true for all product types, including Credit Card accounts.

### For Credit Cards:

*   A positive balance indicates that the customer has credit available to them
    
*   A negative balance indicates funds that the customer owes
    

And as such:

*   The available balances (e.g. `INTERIM_AVAILABLE`) are the funds available to the customer, including the credit made available to them
    
    *   This is expected to always be a positive amount, unless the customer has exceeded the credit made available to them
*   The booked balances (e.g. `INTERIM_BOOKED`) reflect the ‘actual’ balance of the account - i.e. credit funds used minus repayments made.
    
    *   A negative balance reflects that the customer owes funds (i.e. they have used credit that is yet to be repaid)
    *   A positive balance reflects that the bank is holding funds owed to the customer

**For example**

*   An `INTERIM_AVAILABLE` balance of 1000 indicates that a customer has 1000 credit available to them
    
*   An `INTERIM_AVAILABLE` balance of -1000 indicates that the customer has exceeded their available credit by 1000
    
*   An `INTERIM_BOOKED` of -1000 indicates that a customer used 1000 of their credit (and owes this to the bank)