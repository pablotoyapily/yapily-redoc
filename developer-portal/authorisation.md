[](#)

Account Authorisation Flows
===========================

Learn more about the different account authorisation flows in Yapily

Depending on the `Institution` list in your application, you may be required to implement several flows.

Looking for [Payment Authorisations Flows](/#)?

Redirect vs. Embedded
---------------------

_Redirect-based_ account authorisation flows require the PSU to be sent to the domain of the `Institution` to authenticate themself and to securely give their `Consent` to make a request for their financial data.

On the contrary, _embedded_ account authorisation flows take place without any redirect to the `Institution`. This process works by capturing various pieces of information from the PSU through fields in your front-end application and sending this information to the `Institution` through the Yapily API in order to authorise the request for the their financial data.

Coupled Account Authorisations
------------------------------

_Coupled_ account authorisation flows are flows where _every step_ in authorising a request for the PSU's financial data is sent through the Yapily API. This means that at every stage, it is possible to receive the status of the `Consent` without polling it for updates. By default, _all coupled account authorisation flows diagrams display the use case that is suited to customers who are using their own `redirect`_ where after the authorisation step is completed, the PSU is redirected back to the application directly by the `Institution`. See [redirect url](/#) for more information.

For customers using the Yapily `redirectUrl` (`https://auth.yapily.com/`), the `Institution` will redirect the PSU to this url after completing the authorisation which will mean the journey will not end back in your front-end application. As a result, it is recommended that you opt-in to use the `callback` in order to tell Yapily where to redirect the PSU to after being redirected to the authorisation service. See [using a callback](/knowledge/callback_url/#using-a-callback-optional) for more information.

Decoupled Account Authorisations
--------------------------------

_Decoupled_ account authorisation flows are flows where _one step_ in authorising a request for the PSU's financial data takes place without Yapily as the intermediary. In this case, the authorisation is sent directly from the `Institution` to the PSU and back to the `Institution`. As Yapily is not involved in this process, in order for Yapily to check the status of the authorisation, Yapily will first send the `AWAITING_DECOUPLED_AUTHORIZATION` status to signal that the `Institution` has sent a request directly to the PSU to authorise.

As the authorisation is completed and received by the `Institution` without involvement of the Yapily API, you will need to poll [GET Consent](/#) using the `consent-id` in order to know when the PSU has successfully authorised the account authorisation request. Once this occurs, the `Consent` will transition to `AUTHORIZED` signalling that the authorisation process is complete and that you can cease from polling.