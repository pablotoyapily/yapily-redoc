---
title: API Documentation
---

# API Documentation

The Yapily API enables connections between your application and your users' banks to process payments or retrieve financial data. Learn more about how to build with our API, follow our tutorials and gain Open Banking insights here.


Understand how Yapily works and how to get started with integrating Yapily into your application.


## Getting Started

Yapily empowers financial applications to connect with their customers through Open Banking. Our API supports Account Information (AIS) and Payment Initiation services (PIS) that will enrich your customer's experience and create financial opportunity.

### Quickstart

Getting started with Yapily typically takes just a few minutes. After following these steps, you'll be ready to start your integration.

#### 1. Sign-up (1 min)
Sign up here for a free Yapily account.

If you have received an invitation to join an existing Yapily organization, you can access Yapily after signing in.

#### 2. Create an Application (1 min)
After creating an account, you'll need to generate a new Application.

- Go to My Applications and click 'Add New'
- Choose a name for the Application and click 'Create'
- Download the key and secret
- Generate the authToken used to authenticate your requests to your application

#### 3. Configure Callback URL (1 min)
You'll need to configure the Application Callback URL for testing. In the dashboard:

- Select your Application
- Go to the Settings tab and click 'Add Callback'
- Add https://display-parameters.com/ to your authorized callbacks

#### 4 Configure Sandbox (2 mins)
Before going live, you'll want to test your integration to Yapily so you'll need to configure a sandbox. In the dashboard:

- Select your Application
- Go to the Institutions tab and click 'Add Institution'
- Search for modelo-sandbox and click 'Open'
- Add modelo-sandbox to your Application

#### 5 Send your first API request (1 min)
Let's make your first API request to test that you are ready to start your integration.

