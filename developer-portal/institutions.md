---
title: Institutions
description: GitHub-flavored markdown example
toc:
  enable: true
---

Institutions
============

Learn more about how to manage and use Institutions in Yapily.

* * *

API References
--------------

*   [Get Institutions](/openapi/reference/operation/getInstitutions/)
*   [Institution Schema](/openapi/reference/operation/getInstitution/)
*   [Get Features](/openapi/reference/operation/getFeatureDetails/)

Introduction
------------

While Yapily integrates with many banks, within the platform Yapily refers to each of these banks as an `Institution`. This abstraction is necessary because certain bank split up their API endpoints based on personal and business accounts or even according to region e.g. Allied Irish Bank (ROI), Allied Irish Bank Business (ROI), Allied Irish Bank (UK) and Allied Irish Bank Business (UK) are all separate institutions. Similarly, there are several different Institution types that you can use on our platform:

Live Institutions
-----------------

If you have your certificates for AIS and/or PIS ready, you can use any of these `Institutions` with your certificates and access live customer data and initiate payments.

Live vs Beta
------------

Both live and Beta `Institutions` are connections to real accounts that have passed our internal testing. The only difference between the two is that for each Beta integration, we do not currently have any customers using accounts for these banks. As soon we have customers who choose to use these account, the beta integration can be promoted to one that is live.

**NOTE:** When executing [GET Institution](/api/#get-institution) or [GET Institutions](/api/#get-institutions), you can see what type of integration an `Institution` is by looking at the `environmentType` property in the response. The Yapily API currently doesn't distinguish between live and beta integrations (as both appear with the `LIVE` `environmentType`) so the only way to tell between the two currently is through the dashboard.

Configuration
-------------

Get Institutions returns a list of Institutions that are configured against your `Application`. There are three different types of `Institution` that can be setup: `MOCK`, `SANDBOX` and `LIVE`. Both `MOCK` and `SANDBOX` are for testing, while `LIVE` allows for connections to real consumer and business accounts. To use a `LIVE` `Institution` you must register and configure your `Institution` configuration credentials against your application. See [Dashboard](/guides/applications/dashboard/) for more details on configuration and [Registration](/guides/applications/institutions/registration/) for information about `Institution` registration.

Pre-configured Sandboxes
------------------------

These are sandbox environments that come with our certificates ready to use. This is a great option if you want to test Yapily's APIs before using any real banks or data. Yapily recommends that you use the `modelo-sandbox` since it is maintained by the OBIE and receives regular support. While you can use any of the other sandboxes freely, Yapily does not maintain them.

Sandboxes
---------

These are sandbox environments that allow you to use your own certificates. As with the pre-configured sandboxes, be aware that these are not built or maintained by Yapily and in some cases known to be problematic and a poor representation of the institution's live integration.

With the exception of `Yapily Mock (id: yapily-mock)`, `Modelo Sandbox (id: modelo-sandbox)` and `Mock ASPSP (id: mock-sandbox)`, Yapily will never support or fix an issue with a sandbox.

Features
--------

Every `Institution` supports a specific set of features that determine what types of requests they can support, e.g. `CREATE_DOMESTIC_PERIODIC_PAYMENT` vs `ACCOUNT_TRANSACTIONS`.