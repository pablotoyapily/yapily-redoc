---
title: FAQs
---
[](#)

FAQs
====

A range of frequently asked questions and answers.

Open Banking
------------

### How do I know Open Banking is safe?

Open Banking has been designed with security at its heart – here’s how:

*   _Bank-level security_ – Open Banking uses rigorously tested software and security systems. You’ll never be asked to give access to your bank login details or password to anyone other than your own bank or building society.
    
*   _It is regulated_ – Only apps and websites regulated by the FCA or European equivalent can use Open Banking.
    
*   _Fine control_ – The payments service user (PSU) has the ability to control when, and for how long, they give access to their data. The PSU can also revoke this access at any time should they wish to.
    
*   _Extra protection_ – Bank or building societies will pay the PSU back if fraudulent payments occur. PSUs also protected by data protection laws and the Financial Ombudsman Service


## Account Information FAQs

A range of frequently asked questions and answers about account information.

### What are the different account information features?

All the features available in the API are listed in the [FeatureEnum](/api/#tocS_FeatureEnum) object.

The account information features are described in the following [table](/guides/financial-data/features/).

### Does Yapily provide MCCs (merchant category codes) for transactions?


As a gateway, Yapily can only return the information that the institution provides. As MCCs are an optional field for ASPSPs to provide, the presence of this information is completely dependent on whether the `Institution` provides this or not.

If supported by the `Institution`, the `ACCOUNT_TRANSACTIONS_WITH_MERCHANT` feature will be present in the `features` array of the [GET Institution](/#) endpoint.

### Why can I not get identity data from my institution?


As a gateway, Yapily can only return the information that the institution provides. As identify information is optional for ASPSPs to provide, the presence of this information is completely dependent on whether the `Institution` provides this or not.

If supported by the `Institution`, the `IDENTITY` feature will be present in the `features` array of the [GET Institution](#) endpoint.

### Are joint and business accounts supported by Yapily?


Joint accounts and business accounts are accounts that may require multiple authorisations in order to obtain financial data depending on the `Institution`.

In the event that multiple authorisations are required by the `Institution`, once the initiating user authorises the account authorisation request, the state of the `Consent` object will change status from `AWAITING_AUTHORISATION` to `AWAITING_FURTHER_AUTHORIZATION` until the required authorisations have taken place offline e.g. Email, SMS or Phone call.

The `Consent` object will contain information about how many authorisations are required and how many have been authorised while in this state.

### Is pagination supported for obtaining transactions?

Yes, please see [Pagination](/#) for more information.

### Does Yapily support any enrichment?


Yes, please see [Enrichment](/#) for more information.