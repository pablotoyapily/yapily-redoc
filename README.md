# YAPILY PORTAL - REDOC #

## Prerequisites

- node.js >= 12.22.6 and <= 16.x
- yarn

## Install

`yarn install`

## Start development server

`yarn start`

Note: search isn't functional in the development environment.

## Troubleshooting

We heavily rely on caching for performance issues so if some changes are not reflected in the resulting portal try cleaning cache by running:

`yarn clean`


## Docs

Read the official [online documentation](https://redoc.ly/docs/developer-portal/introduction/).

